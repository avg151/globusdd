package com.vladimir.globusdd;

import android.database.Cursor;
import android.os.AsyncTask;

import java.util.Iterator;
import java.util.LinkedList;

import static com.vladimir.globusdd.MyContentProvider.ITEM_ID;
import static com.vladimir.globusdd.MyContentProvider.ITEM_NAME;
import static com.vladimir.globusdd.MyContentProvider.ITEM_NEXT_ID;

public class MyList extends LinkedList<MyItem> {

    private Cursor c;
    private ParseDataListener mListener;

    public void setOnParseDataCompleteListener(ParseDataListener mListener) {
        this.mListener = mListener;
    }

    public interface ParseDataListener {
        void onParseDataComplete(MyList items);
    }

    public MyList() {}

    public MyList(Cursor cursor) {
        c = cursor;
        (new ParseDataTask()).execute();
    }

    class ParseDataTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            //get unsorted
            if (c.getCount() != 0) {
                MyList unsorted = new MyList();
                if (c.moveToFirst()) {
                    do {
                        MyItem cur = new MyItem(
                                c.getInt(c.getColumnIndex(ITEM_ID)),
                                c.getInt(c.getColumnIndex(ITEM_NEXT_ID)),
                                c.getString(c.getColumnIndex(ITEM_NAME)));
                        unsorted.add(cur);
                    } while (c.moveToNext());
                }
                c.close();

                //get first
                LinkedList<Integer> notFirst = new LinkedList<>();
                LinkedList<Integer> all = new LinkedList<>();
                Iterator<MyItem> unsortedIterator;
                unsortedIterator = unsorted.iterator();
                do {
                    MyItem nextItem = unsortedIterator.next();
                    if (nextItem.nextId != -1) {
                        notFirst.add(nextItem.nextId);
                    }
                    all.add(nextItem.id);
                } while (unsortedIterator.hasNext());

                all.removeAll(notFirst);


                //sort
                int nextId = all.getFirst();

                while (nextId != -1) {
                    unsortedIterator = unsorted.iterator();
                    do {
                        MyItem nextItem = unsortedIterator.next();
                        if (nextId == nextItem.id) {
                            add(nextItem);
                            unsortedIterator.remove();
                            nextId = nextItem.nextId;
                            break;
                        }
                    } while (unsortedIterator.hasNext());
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            mListener.onParseDataComplete(MyList.this);
        }
    }
}
