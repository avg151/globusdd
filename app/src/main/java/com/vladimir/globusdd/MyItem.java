package com.vladimir.globusdd;

class MyItem {
    public final int id;
    public int nextId;
    public final String text;

    public MyItem(int id, int nextId, String text) {
        this.id = id;
        this.nextId = nextId;
        this.text = text;
    }
}