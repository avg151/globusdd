package com.vladimir.globusdd;

import android.app.Activity;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.h6ah4i.android.widget.advrecyclerview.draggable.DraggableItemAdapter;
import com.h6ah4i.android.widget.advrecyclerview.draggable.ItemDraggableRange;
import com.h6ah4i.android.widget.advrecyclerview.utils.AbstractDraggableItemViewHolder;

import static com.vladimir.globusdd.MyContentProvider.ITEM_CONTENT_URI;
import static com.vladimir.globusdd.MyContentProvider.ITEM_NEXT_ID;

class MyAdapter extends RecyclerView.Adapter<MyViewHolder>
        implements DraggableItemAdapter<MyViewHolder>,
        MyQueryHandler.AsyncQueryListener,
         MyList.ParseDataListener {

    MyList mItems;
    Context mContext;
    MyQueryHandler handler;
    ProgressBar progressBar;

    public MyAdapter(Context context) {
        setHasStableIds(true);
        mContext = context;
        mItems = new MyList();
        progressBar = (ProgressBar) ((Activity)context).findViewById(R.id.progress_bar);

        handler = new MyQueryHandler(context){};
        handler.setQueryListener(this);
        handler.startQuery(1, null, ITEM_CONTENT_URI, null, null, null, null);
    }

    @Override
    public long getItemId(int position) {
        return mItems.get(position).id;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_for_drag_minimal, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        MyItem item = mItems.get(position);
        holder.textView.setText(item.text);
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    @Override
    public void onMoveItem(int fromPosition, int toPosition) {
        if (fromPosition > toPosition) {
            updateLink(mItems.get(toPosition).id, mItems.get(fromPosition).id, 0);

            if (fromPosition + 1 != mItems.size()) {
                updateLink(mItems.get(fromPosition + 1).id, mItems.get(fromPosition - 1).id, 1);
            } else {
                updateLink(-1, mItems.get(fromPosition - 1).id, 1);
            }

            if (toPosition != 0) {
                updateLink(mItems.get(fromPosition).id, mItems.get(toPosition - 1).id, 2);
            }
        } else {
            updateLink(mItems.get(fromPosition).id, mItems.get(toPosition).id, 3);

            if (fromPosition != 0) {
                updateLink(mItems.get(fromPosition + 1).id, mItems.get(fromPosition - 1).id, 4);
            }

            if (toPosition + 1 != mItems.size()) {
                updateLink(mItems.get(toPosition + 1).id, mItems.get(fromPosition).id, 5);
            } else {
                updateLink(-1, mItems.get(fromPosition).id, 5);
            }
        }

        MyItem movedItem = mItems.remove(fromPosition);
        mItems.add(toPosition, movedItem);
        notifyItemMoved(fromPosition, toPosition);
    }

    private void updateLink(int toItemId, int fromPosition, int token) {
        ContentValues cv = new ContentValues();
        cv.put(ITEM_NEXT_ID, toItemId);
        Uri uri = ContentUris.withAppendedId(ITEM_CONTENT_URI, fromPosition);
        handler.startUpdate(token, null, uri, cv, null, null);
    }

    @Override
    public boolean onCheckCanStartDrag(MyViewHolder holder, int position, int x, int y) {
        return true;
    }

    @Override
    public ItemDraggableRange onGetItemDraggableRange(MyViewHolder holder, int position) {
        return null;
    }

    @Override
    public void onQueryComplete(int token, Object cookie, Cursor cursor) {
        Toast.makeText(mContext, "Query Complete", Toast.LENGTH_SHORT).show();
        new MyList(cursor).setOnParseDataCompleteListener(this);
    }

    @Override
    public void onUpdateComplete(int token, Object cookie, int result) {
        if (result == 1) {
            Toast.makeText(mContext, "Update Complete. Token: " + token, Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(mContext, "Update With Error. Token: " + token, Toast.LENGTH_SHORT).show();
            handler.startQuery(1, null, ITEM_CONTENT_URI, null, null, null, null);
        }
    }

    @Override
    public void onParseDataComplete(MyList items) {
        mItems = items;
        Toast.makeText(mContext, "Parse Complete", Toast.LENGTH_SHORT).show();
        progressBar.setVisibility(View.GONE);
        notifyDataSetChanged();
    }
}

class MyViewHolder extends AbstractDraggableItemViewHolder {
    TextView textView;

    public MyViewHolder(View itemView) {
        super(itemView);
        textView = (TextView) itemView.findViewById(android.R.id.text1);
    }
}