package com.vladimir.globusdd;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.text.TextUtils;

public class MyContentProvider extends ContentProvider {
    static final String DB_NAME = "mydb";
    static final int DB_VERSION = 1;

    static final String ITEMS_TABLE = "items";

    static final String ITEM_ID = "_id";
    static final String ITEM_NEXT_ID = "next_id";
    static final String ITEM_NAME = "name";

    static final String DB_CREATE = "create table " + ITEMS_TABLE + "("
            + ITEM_ID + " integer primary key autoincrement, "
            + ITEM_NEXT_ID + " integer, "
            + ITEM_NAME + " text" + ");";


    static final String AUTHORITY = "com.vladimir.globusdd";

    public static final Uri ITEM_CONTENT_URI = Uri.parse("content://"
            + AUTHORITY + "/" + ITEMS_TABLE);

    static final String ITEM_CONTENT_TYPE = "vnd.android.cursor.dir/vnd."
            + AUTHORITY + "." + ITEMS_TABLE;

    static final String ITEM_CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd."
            + AUTHORITY + "." + ITEMS_TABLE;

    static final int URI_ITEMS = 1;

    static final int URI_ITEMS_ID = 2;

    private static final UriMatcher uriMatcher;
    static {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(AUTHORITY, ITEMS_TABLE, URI_ITEMS);
        uriMatcher.addURI(AUTHORITY, ITEMS_TABLE + "/#", URI_ITEMS_ID);
    }

    DBHelper dbHelper;
    SQLiteDatabase db;

    public boolean onCreate() {
        dbHelper = new DBHelper(getContext());
        return true;
    }

    public Cursor query(Uri uri, String[] projection, String selection,
                        String[] selectionArgs, String sortOrder) {
        switch (uriMatcher.match(uri)) {
            case URI_ITEMS:
                if (TextUtils.isEmpty(sortOrder)) {
                    sortOrder = ITEM_ID + " ASC";
                }
                break;
            case URI_ITEMS_ID:
                String id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    selection = ITEM_ID + " = " + id;
                } else {
                    selection = selection + " AND " + ITEM_ID + " = " + id;
                }
                break;
            default:
                throw new IllegalArgumentException("Wrong URI: " + uri);
        }
        db = dbHelper.getWritableDatabase();
        Cursor cursor = db.query(ITEMS_TABLE, projection, selection,
                selectionArgs, null, null, sortOrder);
        cursor.setNotificationUri(getContext().getContentResolver(),
                ITEM_CONTENT_URI);
        return cursor;
    }

    public Uri insert(Uri uri, ContentValues values) {
        if (uriMatcher.match(uri) != URI_ITEMS)
            throw new IllegalArgumentException("Wrong URI: " + uri);

        db = dbHelper.getWritableDatabase();
        long rowID = db.insert(ITEMS_TABLE, null, values);
        Uri resultUri = ContentUris.withAppendedId(ITEM_CONTENT_URI, rowID);
        getContext().getContentResolver().notifyChange(resultUri, null);
        return resultUri;
    }

    public int delete(Uri uri, String selection, String[] selectionArgs) {
        switch (uriMatcher.match(uri)) {
            case URI_ITEMS:
                break;
            case URI_ITEMS_ID:
                String id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    selection = ITEM_ID + " = " + id;
                } else {
                    selection = selection + " AND " + ITEM_ID + " = " + id;
                }
                break;
            default:
                throw new IllegalArgumentException("Wrong URI: " + uri);
        }
        db = dbHelper.getWritableDatabase();
        int cnt = db.delete(ITEMS_TABLE, selection, selectionArgs);
        getContext().getContentResolver().notifyChange(uri, null);
        return cnt;
    }

    public int update(Uri uri, ContentValues values, String selection,
                      String[] selectionArgs) {
        switch (uriMatcher.match(uri)) {
            case URI_ITEMS:

                break;
            case URI_ITEMS_ID:
                String id = uri.getLastPathSegment();
                if (TextUtils.isEmpty(selection)) {
                    selection = ITEM_ID + " = " + id;
                } else {
                    selection = selection + " AND " + ITEM_ID + " = " + id;
                }
                break;
            default:
                throw new IllegalArgumentException("Wrong URI: " + uri);
        }
        db = dbHelper.getWritableDatabase();
        int cnt = db.update(ITEMS_TABLE, values, selection, selectionArgs);
        getContext().getContentResolver().notifyChange(uri, null);
        return cnt;
    }

    public String getType(Uri uri) {
        switch (uriMatcher.match(uri)) {
            case URI_ITEMS:
                return ITEM_CONTENT_TYPE;
            case URI_ITEMS_ID:
                return ITEM_CONTENT_ITEM_TYPE;
        }
        return null;
    }

    private class DBHelper extends SQLiteOpenHelper {

        public DBHelper(Context context) {
            super(context, DB_NAME, null, DB_VERSION);
        }

        public void onCreate(SQLiteDatabase db) {
            db.execSQL(DB_CREATE);
            ContentValues cv = new ContentValues();
            int from = 1;
            int to = 13000;
            for (int i = from; i < to; i++) {
                if (i == to - 1) {
                    cv.put(ITEM_NEXT_ID, -1);
                } else {
                    cv.put(ITEM_NEXT_ID, i + 1);
                }

                cv.put(ITEM_NAME, "item " + i);
                db.insert(ITEMS_TABLE, null, cv);
            }
        }

        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {}
    }
}
