package com.vladimir.globusdd;

import android.app.Application;
import android.test.ApplicationTestCase;

public class ApplicationTest extends ApplicationTestCase<Application> {
    public ApplicationTest() {
        super(Application.class);
    }

       public void testAdditionIsCorrect() throws Exception {
        assertEquals(4, 2 + 3);
    }
}